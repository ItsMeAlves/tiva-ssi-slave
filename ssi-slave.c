/*********************************************************************************
 *																				 *
 * freertos_demo.c - Simple FreeRTOS example.									 *
 *																				 *
 * Copyright (c) 2012-2017 Texas Instruments Incorporated.  All rights reserved. *
 * Software License Agreement													 *
 * 																				 *
 * Texas Instruments (TI) is supplying this software for use solely and			 *
 * exclusively on TI's microcontroller products. The software is owned by		 *
 * TI and/or its suppliers, and is protected under applicable copyright			 *
 * laws. You may not combine this software with "viral" open-source				 *
 * software in order to form a larger program.									 *
 *																				 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.						 *
 * NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT			 *
 * NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR		 *
 * A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY			 *
 * CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL			 *
 * DAMAGES, FOR ANY REASON WHATSOEVER.											 *
 *																				 *
 * This is part of revision 2.1.4.178 of the EK-TM4C123GXL Firmware Package.	 *
 *																				 *
 *********************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/flash.h"
#include "driverlib/adc.h"
#include "driverlib/ssi.h"
#include "drivers/buttons.h"
#include "utils/uartstdio.h"

#define OFF 0x00
#define RED 0x02
#define BLUE 0x04
#define GREEN 0x08

/******************************************************************************
 * Configure the UART and its pins.  This must be called before UARTprintf(). *
 ******************************************************************************/
void ConfigureUART(void)
{
    // Enable the GPIO Peripheral used by the UART.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Enable UART0
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Configure GPIO Pins for UART mode.
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 115200, 16000000);
}

/******************************************************************************
 * Configure the onboard LED.  This must be called before changing LED state. *
 ******************************************************************************/
void ConfigureLED(void)
{
	// Configure on-board LED and turn it off
	// First, enable the peripheral
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

	// Define output to the 3 possible colors as they are bound to 3 different pins
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
	// Write binary output to these 3 pins
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, OFF);

	// It just wait a little bit (looping)
	SysCtlDelay(20000000);
}

/***************************************
 * Main function, program entry point. *
 ***************************************/
int main(void)
{
	uint8_t ui8ColorSel = 0;
	uint8_t ui8ColorState = 1;
	uint8_t ui8Colors[3] = {RED, GREEN, BLUE};
	uint32_t ui32Message = 0;

    // Set the clocking to run at 50 MHz from the PLL.
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    // Initialize the onboard LED
    ConfigureLED();

    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA4_SSI0RX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_4 | GPIO_PIN_3 | GPIO_PIN_2);

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0, SSI_MODE_SLAVE, 10000, 16);
    SSIEnable(SSI0_BASE);

    // Initialize the UART and configure it
    ConfigureUART();
    UARTprintf("\n******************\n* SSI slave logs *\n******************\n");

    while(1)
    {
    	SSIDataGet(SSI0_BASE, &ui32Message);
    	if(ui32Message == LEFT_BUTTON) {
    		ui8ColorState = !ui8ColorState;
    		if(ui8ColorState)
    			UARTprintf("LEFT_BUTTON received. Changing state to ON.\n");
    		else
    			UARTprintf("LEFT_BUTTON received. Changing state to OFF.\n");
    	}
    	else if(ui32Message == RIGHT_BUTTON) {
    		ui8ColorSel = ++ui8ColorSel % 3;
    		uint8_t color = ui8Colors[ui8ColorSel];

    		if(color == RED) {
    			UARTprintf("RIGHT_BUTTON received. Changing color to RED.\n");
    		}
    		else if(color == GREEN) {
    			UARTprintf("RIGHT_BUTTON received. Changing color to GREEN.\n");
    		}
    		else {
    			UARTprintf("RIGHT_BUTTON received. Changing color to BLUE.\n");
    		}
    	}

    	if(ui8ColorState)
    	{
    		uint8_t color = ui8Colors[ui8ColorSel];
    		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, color);
    	}
    	else
    	{
    		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, OFF);
    	}

    	while(SSIBusy(SSI0_BASE));
    }
}
