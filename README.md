# Tiva C Launchpad SSI Comm - Slave
For master code, please refer to [its repository](https://gitlab.com/ItsMeAlves/tiva-ssi-master).
<br>
<br>
It receives from the SSI peripheral a message that represents a button event from the master controller.
* When the LEFT_BUTTON is pressed, the LED changes its state (from off to on or from on to off).
* When the RIGHT_BUTTON is pressed, the LED changes its color (from a cycle of RED, GREEN and BLUE).

## Board wiring
It's only needed to connect the SSI pins when the board has a common electrical reference.
The configured pins are:

Master | Slave
------ | ------
CLOCK (PA2) | CLOCK (PA2)
FSS (PA3) | FSS (PA3)
TX (PA5) | RX (PA4)
